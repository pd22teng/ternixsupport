/*
 * semControl.h
 *
 *  Created on: May 21, 2015
 *      Author: teng
 */

#ifndef SEMCONTROL_H_
#define SEMCONTROL_H_

int get_sem(const char *pathName, int id, int createFlag);
int init_sem(int sem_id, int init_val);
int del_sem(int sem_id);
int sem_p(int sem_id);
int sem_v(int sem_id);

#endif /* SEMCONTROL_H_ */
