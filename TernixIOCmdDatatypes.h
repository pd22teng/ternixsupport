/*
 * TernixIOCmdDatatypes.h
 *
 *  Created on: May 12, 2015
 *      Author: teng
 */

#ifndef TERNIXIOCMDDATATYPES_H_
#define TERNIXIOCMDDATATYPES_H_

#include <stdint.h>
#include <stdlib.h>
#include <getopt.h>

#define TERNIX_INTPUT  0
#define TERNIX_OUTPUT  1

#define TERNIX_READ		0
#define TERNIX_WRITE	1

#define TERNIX_TEXT_FORMAT	0
#define TERNIX_DEC_FORMAT	1

#define TERNIX_IO_CMD_COUNT	3

#define TERNIX_IO_CMD_MSG_PAIR_FLAG_LEN	2

typedef enum {
	TERNIX_READ_INPUT = 0,
	TERNIX_READ_OUTPUT = 1,
	TERNIX_WRITE_OUTPUT = 2
} TernixIOCmdID;

typedef union {
	uint8_t val;
	struct {
		unsigned id			: 2;
		unsigned io			: 1;
		unsigned ioAction	: 1;
		unsigned cmdFormat	: 1;
		unsigned reserved   : 3;
	} bits;
} TernixIOCmdHeader;

typedef struct {
	const char * shortOptStr;
	const struct option * longopt;
} TernixIOCmdOptions;

typedef struct {
	const char * cmdName;
	const TernixIOCmdHeader * header;
	const TernixIOCmdOptions * options;
} TernixIOCmdItem;

typedef struct {
	uint8_t count;
	const TernixIOCmdItem* items[];
} TernixIOCmdTable;

typedef struct {
	uint32_t ioSelect;
	char falg[TERNIX_IO_CMD_MSG_PAIR_FLAG_LEN];
	uint32_t ioValue;
} TernixIOCmdMessagePair;

typedef struct {
	TernixIOCmdHeader * header;
	uint8_t count;
	TernixIOCmdMessagePair* pairs[];
} TernixIOCmdPacket;

extern const TernixIOCmdTable ternixIOCmdTable;

TernixIOCmdMessagePair* createTernixIOCmdMessagePair(uint32_t ioSelect, char flag, uint32_t ioValue);

#endif /* TERNIXIOCMDDATATYPES_H_ */
